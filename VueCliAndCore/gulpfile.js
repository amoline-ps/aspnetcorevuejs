﻿/// <binding AfterBuild='default' Clean='clean' />
/*
This file is the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/
// References: https://www.typescriptlang.org/docs/handbook/asp-net-core.html

var gulp = require('gulp');
var log = require('fancy-log');
var run = require('gulp-run');
var del = require('del');

gulp.task('watch', function () {
    log("Watch is not working yet. Run [npm run watch] manually");

    // return run('npm run watch').exec();
    //return gulp.watch('client-app/**/*',
    //    function() {
    //        return run('npm run build:dev').exec();
    //    }).on('change',
    //    function(file) {
    //        log('Vue File Changed ' + JSON.stringify(file));
    //    });

    return true;
});

gulp.task('clean', function() {
    return del(['wwwroot/app/**/*']);
});

gulp.task('build',
    function() {
        return run('npm run build:dev').exec();
    });

gulp.task('default', gulp.series('clean', 'build'));