# Information
This is a Proof of concept to get VueJS working within a hosted Razor Page.

## Running the Solution
- Clone Repository
- Build And Run Solution
- From `client-app` folder, run `npm i` to install the dependencies
- Browse to: [SequenceManagement](http://localhost:52186/SequenceManagement#/) to see the client side application.

## TODO
- The watch command doesn't work with gulp since we are running the command from another packge. You will need to run it manully. 
- Get the build command to install and run 
# References
- https://docs.microsoft.com/en-us/aspnet/core/client-side/using-grunt?view=aspnetcore-3.1