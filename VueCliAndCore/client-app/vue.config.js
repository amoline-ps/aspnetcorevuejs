﻿// vue.config.js
module.exports = {
  outputDir: "../wwwroot/app/",
  // This url would be if you hit it directly. We should ignore it.
  publicPath: "/app/",
  filenameHashing: false
}